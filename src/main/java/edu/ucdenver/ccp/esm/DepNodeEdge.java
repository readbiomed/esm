package edu.ucdenver.ccp.esm;

public class DepNodeEdge extends Edge<DepNodeVertex> {

	public DepNodeEdge(DepNodeVertex gov, String label, DepNodeVertex dep) {
		super(gov, label, dep);
	}

}
