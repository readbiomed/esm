package edu.ucdenver.ccp.esm;

public interface Vertex {

	/**
	 * retrieve the comparison form of the node
	 * @return comparison form
	 */
	public abstract String getCompareForm();

}